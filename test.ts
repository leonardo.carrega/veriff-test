import page from './pages/mainPage';
import verification from './pages/verificationLightBoxPage'

fixture `Fill the form`
    .page `https://demo.veriff.me/`;

    test('Static Happy path', async () => {
        await page.selectLanguage('English');
        await page.selectCountry('United States of America');
        await page.selectDocumentType('Passport');
        await page.selectLaunchVia('redirect');
        await page.clickOnVeriffMeButton();
        await verification.checkPopUpDisplayed();
    });

    // Will get some error 400 on response.
    
    test('Random Happy path', async () => {
        await page.selectRandomLanguage();
        await page.selectRandomCountry();
        await page.selectRandomDocumentType();
        await page.clickOnVeriffMeButton();
        await verification.checkPopUpDisplayed();
    });

    // This is a three dimensional matrix to check every combination,
    // When an error is displayed, the 3 elements selected that cause the error could be saved in the csv.
    // This code breaks to avoid a slow process, the code is only to show the idea.
    test('Print combination with error', async () => {
        await page.checkAllPosibilities();
    });

