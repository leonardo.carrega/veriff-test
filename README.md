# Automation testing for Veriff

This automation was made using Testcafe, you can visit https://testcafe.io for more info.

## How to run it locally

### Clone this public repository
```text
git clone https://gitlab.com/leonardo.carrega/veriff-test.git
```

### Install dependencies
```text
npm install
```

if you have any issue add ...

```text
npm install -g testcafe
```

### Run testing scripts

```text
npm run test
```

or a simple...
```text
testcafe chrome
```

Regards!
