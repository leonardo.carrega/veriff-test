import { Selector, t } from 'testcafe';
import { randomIntFromInterval } from '../commons';
import verification from './verificationLightBoxPage';

class Page {

    selectedLanguage: string;
    selectedCountry: string;
    selectedDocument: string;

    private readonly fullName: Selector = Selector('input').withAttribute('name', 'name');
    private readonly sessionLanguageDropdown: Selector = Selector('button').withAttribute('name', 'language');
    private readonly sessionLanguageOptions: Selector = Selector('.Select-module_option__2ojUk'); // 40
    private readonly documentCountryDropdown: Selector = Selector('.Autocomplete-module_iconButton__1WL4T');
    private readonly documentCountryOptions: Selector = Selector('.Autocomplete-module_option__3XYuT'); // 250
    private readonly documentTypeDropdown: Selector = Selector('button').withAttribute('name', 'documentType');
    private readonly documentTypeOptions: Selector = Selector('.Select-module_option__2ojUk');    
    private readonly launchVeriffVia: Selector = Selector('input').withAttribute('name', 'launchVia');
    private readonly veriffMeButton: Selector = Selector('.submit')

    public async selectLanguage(languageName){
        await t.click(this.sessionLanguageDropdown);
        await t.click(this.sessionLanguageOptions.withExactText(languageName));
        this.selectedLanguage = languageName;
    }

    public async selectCountry(countryName){
        await t.click(this.documentCountryDropdown);
        await t.click(this.documentCountryOptions.withExactText(countryName));
        this.selectedCountry = countryName;
    }
    
    public async selectDocumentType(documentType){
        await t.click(this.documentTypeDropdown);
        await t.click(this.documentTypeOptions.withExactText(documentType));
        this.selectedDocument = documentType;
    }

    public async selectLanguageByPosition(position){
        await t.click(this.sessionLanguageDropdown);
        this.selectedLanguage = await this.sessionLanguageOptions.nth(position).innerText;
        await t.click(this.sessionLanguageOptions.nth(position));
    }

    public async selectCountryByPosition(position){
        await t.click(this.documentCountryDropdown);
        this.selectedCountry = await this.documentCountryOptions.nth(position).innerText;
        await t.click(this.documentCountryOptions.nth(position));
    }
    
    public async selectDocumentTypeByPosition(position){
        await t.click(this.documentTypeDropdown);
        this.selectedDocument = await this.documentTypeOptions.nth(position).innerText;
        await t.click(this.documentTypeOptions.nth(position));
    }


    public async selectRandomLanguage(){
        const randomNum = randomIntFromInterval(0, 39) // There are 40 languages;
        console.log(randomNum)
        await t.click(this.sessionLanguageDropdown);
        this.selectedLanguage = await this.sessionLanguageOptions.nth(randomNum).innerText;
        await t.click(this.sessionLanguageOptions.nth(randomNum));
        console.log(this.selectedLanguage)
    }

    public async selectRandomCountry(){
        const randomNum = randomIntFromInterval(0, 249) // There are 250 countries;
        console.log(randomNum)
        await t.click(this.documentCountryDropdown);
        this.selectedCountry = await this.documentCountryOptions.nth(randomNum).innerText;
        await t.click(this.documentCountryOptions.nth(randomNum));
        console.log(this.selectedCountry)
    }

    public async selectRandomDocumentType(){
        const randomNum = randomIntFromInterval(0, 3) // There are 3 documentType;
        await t.click(this.documentTypeDropdown);
        this.selectedDocument = await this.documentTypeOptions.nth(randomNum).innerText;
        await t.click(this.documentTypeOptions.nth(randomNum));
        console.log(this.selectedDocument)
    }

    public async selectLaunchVia(via){
        await t.click(this.launchVeriffVia.withAttribute('value', via))
    }

    public async clickOnVeriffMeButton(){
        await t.click(this.veriffMeButton)
    }

    public async checkAllPosibilities(){
        // Type 
        await t.click(this.documentTypeDropdown)
        for( var i = 0; i < await this.documentTypeOptions.count; i++){
            this.selectedDocument = await this.documentTypeOptions.nth(i).innerText;
            await t.click(this.documentTypeOptions.nth(i));
            
            // Language 
            await t.click(this.sessionLanguageDropdown)
            for( var i = 0; i < await this.sessionLanguageOptions.count; i++){
                this.selectedLanguage = await this.sessionLanguageOptions.nth(i).innerText;
                await t.click(this.sessionLanguageOptions.nth(i));
                // Country
                await t.click(this.documentCountryDropdown);
                for (var i = 0; i < await this.documentCountryOptions.count; i++){
                    await t.click(this.documentCountryOptions.nth(i));
                    await this.clickOnVeriffMeButton();
                    try {
                        await verification.closeLightBox();
                    }
                    catch {
                        // This could be save in a csv file
                        console.log(`Error on language: ${this.selectLanguage}, country: ${this.selectCountry}and document type: ${this.selectDocumentType}`);
                    }
                    finally {
                        await t.navigateTo("https://demo.veriff.me/");
                    }
                }
            }
        }            // Perform some validation/comparision on the text to see if it is as you expect.... 
    }
}

export default new Page();