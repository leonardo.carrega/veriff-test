import { Selector, t } from 'testcafe';
import mainpage from './mainPage'
class VerificationLightBoxPage {
    private readonly logo: Selector = Selector('img').withAttribute('src', '/static/images/themes/default/logo.svg');
    private readonly closeButton: Selector = Selector('button').withAttribute('data-test-leave-session-button', 'true');
    private readonly phoneNumberInput: Selector = Selector('#phone')

    public async checkPopUpDisplayed(){
        await t.expect(this.phoneNumberInput.exists)
        .ok(`Selected language: ${mainpage.selectLanguage}, country: ${mainpage.selectCountry}and document type: ${mainpage.selectDocumentType}`,
        {timeout: 5000});
        }

    public async closeLightBox(){
        await t.wait(5000)
        await t.click(this.closeButton);
    }
    }

export default new VerificationLightBoxPage();